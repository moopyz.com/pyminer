import numpy

from .exceptions import LinearAlgebraError


def matrix_diagonal(arr: numpy.ndarray, k: int = 0) -> numpy.ndarray:
    assert isinstance(arr, numpy.ndarray), 'param 1 should be `numpy.ndarray`'
    assert isinstance(k, int), 'param 2 should be `int`'
    try:
        return numpy.diag(arr, k)
    except ValueError as e:
        if e.args[0] == 'Input must be 1- or 2-d.':
            raise LinearAlgebraError('only 1d array or 2d array supported')
        raise
