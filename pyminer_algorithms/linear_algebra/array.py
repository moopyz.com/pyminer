import numpy


def array(data, type=None, dtype=None):
    if type is None:
        if dtype is None:
            cleaned_type = None
        else:
            cleaned_type = dtype
    else:
        if dtype is None:
            cleaned_type = type
        else:
            assert False, 'specify both `type` and `dtype` is not allowed'
    return numpy.array(data, dtype=cleaned_type)
