<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>Form</name>
    <message>
        <location filename="../ui/axes_control.py" line="369"/>
        <source>Form</source>
        <translation>设置</translation>
    </message>
    <message>
        <location filename="../ui/axes_control.py" line="378"/>
        <source>X  range?</source>
        <translation type="obsolete">X轴范围：</translation>
    </message>
    <message>
        <location filename="../ui/axes_control.py" line="398"/>
        <source>start</source>
        <translation>开始</translation>
    </message>
    <message>
        <location filename="../ui/axes_control.py" line="401"/>
        <source>(%.2f,%.2f)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/axes_control.py" line="406"/>
        <source>(0,0)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/axes_control.py" line="370"/>
        <source>确定</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/axes_control.py" line="371"/>
        <source>取消</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/axes_control.py" line="372"/>
        <source>应用</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/axes_control.py" line="373"/>
        <source>X 标签：</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/axes_control.py" line="374"/>
        <source>Y 标签：</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/axes_control.py" line="375"/>
        <source>Z 标签：</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/axes_control.py" line="376"/>
        <source>主标题：</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/axes_control.py" line="377"/>
        <source>子标题：</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/axes_control.py" line="378"/>
        <source>X  range：</source>
        <translation>X轴范围：</translation>
    </message>
    <message>
        <location filename="../ui/axes_control.py" line="379"/>
        <source>Y  range：</source>
        <translation>Y轴范围：</translation>
    </message>
    <message>
        <location filename="../ui/axes_control.py" line="380"/>
        <source>Z range：</source>
        <translation>Z轴范围：</translation>
    </message>
    <message>
        <location filename="../ui/axes_control.py" line="381"/>
        <source>X  major  locator：</source>
        <translation>X轴主刻度：</translation>
    </message>
    <message>
        <location filename="../ui/axes_control.py" line="382"/>
        <source>Y  major  locator：</source>
        <translation>Y轴主刻度：</translation>
    </message>
    <message>
        <location filename="../ui/axes_control.py" line="383"/>
        <source>Z  major  locator：</source>
        <translation>Z轴主刻度：</translation>
    </message>
    <message>
        <location filename="../ui/axes_control.py" line="384"/>
        <source>X  minor  locator：</source>
        <translation>X轴次刻度：</translation>
    </message>
    <message>
        <location filename="../ui/axes_control.py" line="385"/>
        <source>Y minor  locator：</source>
        <translation>Y轴次刻度：</translation>
    </message>
    <message>
        <location filename="../ui/axes_control.py" line="386"/>
        <source>Z minor  locator：</source>
        <translation>Z轴次刻度：</translation>
    </message>
    <message>
        <location filename="../ui/axes_control.py" line="387"/>
        <source>坐标</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/axes_control.py" line="388"/>
        <source>显示轴：</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/axes_control.py" line="389"/>
        <source>网格颜色：</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/axes_control.py" line="390"/>
        <source>刻度：</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/axes_control.py" line="391"/>
        <source>网格样式：</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/axes_control.py" line="392"/>
        <source>网格宽度：</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/axes_control.py" line="393"/>
        <source>网格</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/axes_control.py" line="394"/>
        <source>中文字体：</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/axes_control.py" line="395"/>
        <source>英文字体：</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/axes_control.py" line="396"/>
        <source>中英文混合字体：</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/axes_control.py" line="397"/>
        <source>重新检索字体库：</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/axes_control.py" line="399"/>
        <source>字体</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/axes_control.py" line="400"/>
        <source>坐标样式：</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/axes_control.py" line="402"/>
        <source>背景颜色：</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/axes_control.py" line="403"/>
        <source>边框颜色：</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/axes_control.py" line="404"/>
        <source>边框粗细：</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/axes_control.py" line="405"/>
        <source>文字偏移量：</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/axes_control.py" line="407"/>
        <source>箭头粗细：</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/axes_control.py" line="408"/>
        <source>箭头颜色：</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/axes_control.py" line="409"/>
        <source>箭头形状：</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/axes_control.py" line="410"/>
        <source>是否显示点：</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/axes_control.py" line="411"/>
        <source>文字大小：</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/axes_control.py" line="412"/>
        <source>文字颜色：</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/axes_control.py" line="413"/>
        <source>是否显示文字：</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/axes_control.py" line="414"/>
        <source>是否显示箭头：</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/axes_control.py" line="415"/>
        <source>注释</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/axes_control.py" line="416"/>
        <source>输出图像分辨率/dpi：</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/axes_control.py" line="417"/>
        <source>宽</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/axes_control.py" line="418"/>
        <source>高</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/axes_control.py" line="419"/>
        <source>输出图像尺寸/英寸：</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/axes_control.py" line="420"/>
        <source>切换Tab页绘图方式：</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/axes_control.py" line="421"/>
        <source>选择默认绘图风格：</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/axes_control.py" line="422"/>
        <source>1. 图像的大小和分辨率只有在保存时生效
2. 图像像素数=图片尺寸/inch×分辨率/dpi
3. 绘图风格采用SciencePlots包，只会在重新plt.show()后才会生效，文字中含有
中文可能会报错，默认字体的设置将会无效</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/axes_control.py" line="426"/>
        <source>绘图</source>
        <translation></translation>
    </message>
</context>
</TS>
