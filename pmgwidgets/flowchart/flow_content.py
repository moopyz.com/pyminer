from PyQt5.QtCore import QObject, pyqtSignal
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from pmgwidgets.flowchart.flow_node import Node
    from pmgwidgets.flowchart.flowchart_widget import PMGraphicsScene


class FlowContent(QObject):
    signal_exec_started = pyqtSignal(str)
    signal_exec_doing = pyqtSignal(str)
    signal_exec_finished = pyqtSignal(str)

    def __init__(self, node, code: str):
        super().__init__()
        from .flow_node import Node
        self.signal_exec_doing.connect(lambda x:print(123123))
        self.input_args = []
        self.results = []
        self.node: Node = node
        self.node.content = self
        if code == '':
            code = """
import time
def function(x,y):
    time.sleep(1)
    return y*2,x+2
print(function(1,2))
        """
        self.code = code

    def get_settings_params(self):
        return [('text_edit', 'code', 'Input Python Code', self.code, 'python')]

    def update_settings(self, settings_dic: dict):
        self.code = settings_dic['code']

    def _process(self):
        input_port_list = [p.get_connected_port()[0] for p in self.node.input_ports]
        # output_port_list = [p.get_connected_port() for p in self.node.output_ports]
        input_list = []
        try:
            for p in input_port_list:
                index = p.node.get_port_index(p)
                print(index)
                val = p.node.content.results[index]
                input_list.append(val)
        except:
            import traceback
            traceback.print_exc()
            return
        self.signal_exec_started.emit('started,\ninput value:%s' % (repr(input_list)))
        result = self.process(*input_list)
        print(result)
        self.results = result
        next_content = self.get_next_content()
        print('next_content', next_content)
        self.signal_exec_finished.emit(
            'finished\ninput value:%s\noutput value:%s' % (repr(input_list), repr(self.results)))
        if next_content is None:
            return
        else:
            next_content._process()
        # self.signal_exec_finished.emit()

    def process(self, *args):
        # print('executing', globals())
        # locals().update({'self':self})
        globals().update({'self':self})
        exec(self.code, globals())
        return function(*args)

    def dump(self):
        return {'code': self.code}

    def get_next_content(self) -> 'FlowContent':
        scene: 'PMGraphicsScene' = self.node.base_rect.scene()
        scene.node_index_to_execute += 1
        node_index = scene.node_index_to_execute
        if node_index >= len(scene.call_id_list):
            scene.node_index_to_execute = 0
            return None
        node_id = scene.call_id_list[node_index]
        return scene.find_node(node_id).content


if __name__ == '__main__':
    def process():
        self = 123455
        code = """
def function():
    print(123)
        """
        # print('executing', globals())
        # locals().update({'self':self})
        loc = locals()
        exec(code)
        print(loc['function'])


    process()
#     f = lambda: print(123)
#     code = """
# def abc(x,y,z):
#     for i in range(10):
#         f()
#     return x,y+z
#     """
#     exec(code)
#
#     print(abc(1, 2, 3))
